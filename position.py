import time


'''index board
 | 0 | 1 |  2|  3|  4|  5|  6|  7|
 *********************************
 | 8 | 9 | 10| 11| 12| 13| 14| 15|
 *********************************
 | 16| 17| 18| 19| 20| 21| 22| 23|
 *********************************
 | 24| 25| 26| 27| 28| 29| 30| 31|
 *********************************
 | 32| 33| 34| 35| 36| 37| 38| 39|
 *********************************
 | 40| 41| 42| 43| 44| 45| 46| 47|
 *********************************
 | 48| 49| 50| 51| 52| 53| 54| 55|
 *********************************
 | 56| 57| 58| 59| 60| 61| 62| 63|
 *********************************
'''

class Position(object):
    
   
    X_OFFSET = [0,   1, 1, 1, 0, -1, -1, -1]
    Y_OFFSET = [-1, -1, 0, 1, 1,  1,  0, -1]
    EMPTY = 0
    WHITE = 1
    BLACK = 2

    evaluation_table = [
            200, -30, 11, 8, 8, 11, -30, 200,
		     -30, -7, -4, 1, 1, -4, -7, -30,
		      11, -4, 2, 2, 2, 2, -4, 11,
		       8, 1, 2, -3, -3, 2, 1, 8,
		       8, 1, 2, -3, -3, 2, 1, 8,
		      11, -4, 2, 2, 2, 2, -4, 11,
		     -30, -7, -4, 1, 1, -4, -7, -30,
		      200, -30, 11, 8, 8, 11, -30, 200
            ]    
    
    INIT_BOARD = '0000000000000000000000000001200000021000000000000000000000000000'

    def __init__(self, init_str=None, turn=1, skip=False):
        if not skip:
            self.turn = turn
            self.setup(self.INIT_BOARD if init_str is None else self.init_str)
            
    def copy(self):
        c = Position(None, 1, True)
        c.turn = self.turn
        c.board = self.board[:]
        c.count = self.count[:]    
        return c
        
    def setup(self, bstr):
        if len(bstr) != 64:
            print('Board string length {}'.format(len(bstr)))
        else:
            self.board = [ int(c) for c in bstr ]
            self.count = [ 0, 0, 0 ]
            for c in self.board:
                self.count[c] += 1
    
    def change_list(self, m):
        # collect changing piece indexes in list
        cplist = []
        oppturn = 1 if self.turn == 2 else 2
        x, y = m%8, m//8
        for dx, dy in [ (1, 0), (-1, 0), (0, 1), (0, -1), (1, 1), (-1, -1), (-1, 1), (1, -1) ]:
            d, dlist = 1, []
            while 0 <= (x+d*dx) and (x+d*dx) < 8 and 0 <= (y+d*dy) and (y+d*dy) < 8:
                bi = (y+d*dy)*8 + (x+d*dx)
                if self.board[bi] != oppturn:
                    if self.board[bi] == self.turn:
                        cplist += dlist
                    break
                dlist.append(bi)
                d += 1
        return cplist

    def move(self, m):        
        # play move, change pieces, update counts and change player's turn
        self.board[m] = self.turn
        oppturn = 1 if self.turn == 2 else 2
        for bi in self.change_list(m):
            self.board[bi] = self.turn
            self.count[self.turn] += 1 
            self.count[oppturn] -= 1 
        self.count[0] -= 1 
        self.turn = 1 if self.turn == 2 else 2      

    '''
    Evaluacijska metoda
    '''
    def evaluate(self):
        total = 0
        for i in self.board:
            if self.board[i] == self.BLACK:
                total+=self.evaluation_table[i]
            elif self.board[i] == self.WHITE:
                total-=self.evaluation_table[i]
        return total * 20
    
    '''
    vraća tip pločice sa table
    '''
    def getDisc(self, index):
        # returns type of disc on board
        if self.board[index] == 1:
            return self.WHITE
        if self.board[index] == 2:
            return self.BLACK
        
        return self.EMPTY
    
    '''
    rekursivna funkcija koja ispituje položaje po offsetu
    '''
    def posibleMove(self,index,direction,depth):

        x = index%8 + self.X_OFFSET[direction]
        y = index//8 + self.Y_OFFSET[direction]
        if x<0 or x>=8 or y < 0 or y>=8:
            return False
        
        index = 8*y+x
       
        if self.getDisc(index) == self.EMPTY:
            return False

        tmp = self.getDisc(index)
        
        if tmp == self.turn :
                return depth > 0

        possible = self.posibleMove(index,direction,depth+1)
        
        return possible

    '''
    detektira da li je moguće odigrati potez
    '''
    def isPossible(self, index):

        if self.getDisc(index) != self.EMPTY:
            return False
        for i in range(8):
            if self.posibleMove(index,i,0):
                return True
        return False
    
    '''
    vraća listu legalnih
    '''
    def legal_moves(self):
#        return [ m for m in range(64) if self.board[m] == 0 ]
        lmoves = []
        index = 0
        for i in range(8):
            for j in range(8):
                if self.isPossible(index)==True:
                    lmoves.append(index)
                    index = index + 1
                    continue
                index = index + 1
        return lmoves
    
    '''
    vraća listu legalnih dodatno printa u konzolu bot name
    '''	    
    def legal_moves_name(self, name):
        
        lmoves = []
        print(name)
#        return [ m for m in range(64) if self.board[m] == 0 ]
        index = 0
        for i in range(8):
            for j in range(8):
                if self.isPossible(index)==True:
                    lmoves.append(index)
                    index = index + 1
                    continue
                index = index + 1
        return lmoves
    
    '''
    printa tablu sa zauzetim 1 i 2 te mogućim X
    '''
    def legal_moves_print(self):
#        lmoves = []
        print('    0   1   2   3   4   5   6   7')
        index = 0
        for i in range(8):
            print(i,end= '')
            for j in range(8):
                if self.isPossible(index)==True:
                    print('   X',end= '')
#                    lmoves.append(index)
                    index = index + 1
                    continue
                
                if self.board[index] == self.BLACK:
                    print('   2',end= '')
                    index = index + 1
                    continue
                    
                if self.board[index] == self.WHITE:
                    print('   1',end= '')
                    index = index + 1
                    continue
                    
                if self.board[index] == self.EMPTY:
                    print('    ',end= '')
                    index = index + 1 
                    continue
                    
#                index = index + 1
            print('')

    def is_legal(self, m):
        return self.board[m] == 0

    def tostring(self):
        return str(self.turn), ''.join(str(c) for c in self.board)
    
    def move_count(self):
        return 64 - self.count[0]
        
    def game_over(self):
        # 0 - still playing, 1 or 2 - victory, 3 - draw
        if self.count[0] > 0:
            return 0
        elif self.count[1] > self.count[2]:
            return 1
        elif self.count[2] > self.count[1]:
            return 2
        else:
            return 3            

if __name__ == '__main__':
    pos = Position(None,1,False)
#    l = pos.legal_moves()
    
    while True:
        print(pos.tostring())
        l = pos.legal_moves_name("WHITE")
       # l = pos.legal_moves_print()
        #print(l)
#        x = int(input('enter x:'))
#        y = int(input('enter y:'))
#        index = 8 *y+x
#        print(index)
        if pos.turn == pos.BLACK:
            print('BLACK ',end='')
        if pos.turn == pos.WHITE:
            print('WHITE ',end='')
#        m = int(input('enter move:'))
        pos.move(l[0])
        time.sleep(3)
        
        pos.legal_moves_print()
   
        
        
    
    print(pos.tostring())
    