import sys,  copy
from position import Position

class MinMaxBot(object):
    
    DEPTH  = 4
    MAX_VAL = 10000000
    MIN_VAL = -10000000
    

       
    def __init__(self):
        self.pos = Position()
        self.time_for_move = None
        self.move = 0
        self.node_cnt = 0
    
    def output(self, cstr):
        sys.stdout.write(cstr)
        sys.stdout.flush()
        
    def error(self, cstr):
        sys.stderr.write(cstr)
        sys.stderr.flush()
        
    def command(self, cstr):
        if cstr.startswith('update position'):
            self.pos.setup(cstr[16:-1])
        elif cstr.startswith('update turn'):
            self.pos.turn = int(cstr[12])
        elif cstr.startswith('action'):
            self.time_for_move = int(cstr[7:])
            move = self.best_move()
            self.output('move ' + str(move) + '\n')
        elif cstr.startswith('quit'):
            quit()
        else:
            self.error('unknown command ' + cstr + '\n')
        return True
    

    '''
    MinMax metoda
    '''
    def minimax(self, p, d):
        self.node_cnt += 1

        if d==0 or p.game_over()!=0:
            return (p.evaluate(),None)
            

        best_val = -self.MAX_VAL if p.turn == p.BLACK else self.MAX_VAL
        best_move = None
        for m in p.legal_moves_name('Minimax '+ str(self.pos.turn)):
            pos = copy.deepcopy(p)
            pos.move(m)
            val, _ = self.minimax(pos, d-1)
            
            if pos.turn == self.pos.WHITE:
                if val > best_val:
                    best_val = val
                    best_move = m
            elif pos.turn == self.pos.BLACK:

                if val < best_val:
                    best_val = val
                    best_move = m
                    
        print('Move:', best_move, 'Nodes:', self.node_cnt)
        return (best_val, best_move)
    
    '''
    AlphaBeta metoda
    '''
    def alphabeta(self,p,alpha,beta, d):
        
        self.node_cnt+=1

                
        if d==0 or p.game_over()!=0:
            return (p.evaluate(),None)

        best_move = None

        for m in p.legal_moves_name('Minimax '+ str(self.pos.turn)):
            pos = copy.deepcopy(p)
            pos.move(m)
            val, _ = self.alphabeta(pos, alpha, beta, d-1)
            
            if pos.turn == self.pos.WHITE:
                if val >= alpha:
                    alpha = val
                    best_move = m
                    
            elif pos.turn == self.pos.BLACK:
                if val <= beta:
                    beta = val
                    best_move = m
                    
            if alpha >= beta:
                break
        print('Move:', best_move, 'Nodes:', self.node_cnt)

        if p.turn == p.WHITE:
            return (alpha, best_move)
        else:
            return (beta, best_move)
        
       
  
        
    def best_move(self):
        self.node_cnt = 0
#        moves = self.pos.legal_moves()
#        return moves[len(moves)-1]
#        moves = self.pos.legal_moves('MINIMAX '+ str(self.pos.turn))
#        val, move = self.alphabeta(self.pos,self.MIN_VAL,self.MAX_VAL,self.DEPTH)
        val, move = self.minimax(self.pos,self.DEPTH)
        return move
    
    
if __name__ == '__main__':
    bot = MinMaxBot()        
    while True:
        cstr = sys.stdin.readline()
        print(cstr)
        bot.command(cstr)
        
